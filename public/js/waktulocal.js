var tanggal ="";
var hari = "";
var bulan="";
var waktu=""
var time = new Date();

switch (time.getDay()) {
  case 0:
    hari="minggu"
    break;

    case 1:
    hari="senin"
    break;

    case 2:
    hari="selasa"
    break;

    case 3:
    hari="rabu"
    break;

    case 4:
    hari="kamis"
    break;

    case 5:
    hari="jumat"
    break;

    case 6:
    hari="sabtu"
    break;

  default:
    break;
}

switch (time.getMonth()) {
  case 0:
    bulan='januari'
    break;

    case 1:
    bulan='februari'
    break;

    case 2:
    bulan='maret'
    break;

    case 3:
    bulan='april'
    break;

    case 5:
    bulan='mei'
    break;

    case 6:
    bulan='juni'
    break;

    case 7:
    bulan='juli'
    break;

    case 8:
    bulan='agustus'
    break;

    case 9:
    bulan='september'
    break;
    

    case 10:
      bulan='oktober'
      break;


      case 11:
        bulan='november'
        break;

        case 12:
          bulan='desember'
          break;

  default:
    break;
}


if(time.toLocaleTimeString().search("AM")>1){
  if(time.toLocaleTimeString()[0]>=1 && time.toLocaleTimeString()[0]<=12){
    waktu = "Pagi"
  }
}else if(time.toLocaleTimeString().search("PM")>1){
  if(time.toLocaleTimeString()[0]>=1 && time.toLocaleTimeString()[0]<4){
    waktu = "Siang"
  }else if(time.toLocaleTimeString()[0]>=5 && time.toLocaleTimeString()[0]<8){
    waktu = "Petang"
  }else{
    waktu="malam"
  }
}

tanggal = `${hari}, ${time.getDate()} ${bulan} ${time.getFullYear()} pukul ${time.toLocaleTimeString().slice(0, -6)} ${waktu}`

console.log(tanggal)
document.getElementById('display').innerHTML=tanggal