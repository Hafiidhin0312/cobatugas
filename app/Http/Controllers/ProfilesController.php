<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\User;
use App\Friendship;
use App\Post;
use Auth;
use Session;
use DB;

class ProfilesController extends Controller
{
    
	public function index($slug)
	{
		$user = User::where('slug', $slug)->first();
		
		$follower = Friendship::where('user_requested',$user->id)->orwhere('requester',$user->id)->where('status','=',1)->count();
		$following = Friendship::where('requester',$user->id)->count();
		$post = Post::where('user_id',$user->id)->count();


		return view('profiles.profile')
			   ->with('user', $user)
				 ->with('follower', $follower)
				 ->with('following', $following)
				 ->with('post', $post);
	}

	public function edit()
	{
		return view('profiles.edit')->with('info', Auth::user()->profile);
	}

	public function update(Request $request)
	{
		$this->validate($request, [
			'location' => 'required',
			'about'	   => 'required|max:255'
		]);

		Auth::user()->profile()->update([
			'location' => $request->location,
			'about'	   => $request->about
		]);

		if ( $request->hasFile('avatar') ) {
			$name = Auth::user()->name;
			Auth::user()->update([


				$file=$request->file('avatar'),
				$namafile = $name.".png",
				'avatar' => $request->avatar->storeAs('avatars',$namafile),
				$tujuan_upload = 'avatars',
	    	$file->move($tujuan_upload,$namafile)
			]);
		}

		Alert::success('Berhasil', 'Data Terupdate');
		//return redirect()->back();
		return view('/home');
	}



	public function friend($id)
	{
		if ($id == Auth::id() ) {
			$friends = Auth::user()->friends();
		} else {
			$friends = Auth::user()->friends($id);
		}

		return $friends;
	}

}
