@extends('layouts.app')

@section('content')
	
<div class="container">
	<div class="row">
		<div class="col-lg-4">
			<div class="panel panel-primary">
				<div class="panel-heading">
					{{ $user->name }}'s profile
					
				</div>
				<div class="panel-body">
				
					<center>
						<img src="{{asset('/')}}{{ $user->avatar }}" width="140px" height="140px" style="border-radius: 50%">
					</center>

					<h2 class="text-center">{{ $user->name }}</h2>

					<br>
					<p class="text-center">
						<i class="fa fa-map-marker fa-2x" aria-hidden="true" style="color:red">&nbsp;</i>{{ $user->profile->location }}
					</p>
					<br>

					<p class="text-center">
						
						@if(Auth::id() == $user->id)

							<a href="{{ route('profile.edit') }}" style="background-color: #3097D1; color:white; padding:10px; text-decoration:none">Edit your profile</a>

						@endif

					</p>
					
					<div style= "float:left">
						<h3>Follower</h3>
						<h1 class="text-center" >{{$follower}}</h1>
					</div>

					<div style="float:left; margin-left:20px">
						<h3>Following</h3>
						<h1 class="text-center">{{$following}}</h1>
					</div>

					<div style="float:left; margin-left:20px">
						<h3>Post</h3>
						<h1 class="text-center">{{$post}}</h1>
					</div>

				
				</div>
			</div>
			@if (Auth::id() != $user->id)
				<div class="panel panel-primary">
					<div class="panel-body">
						<friend :profile_user_id="{{ $user->id }}"></friend>
					</div>
				</div>
			@endif

			<div class="panel panel-primary">
				<div class="panel-heading">
					About me
				</div>
				<div class="panel-body">
						
					{{ $user->profile->about }}

				</div>
			</div>

			<friendlist :id="{{ $user->id }}"></friendlist>

		</div>

		<div class="col-lg-8">
			@if(Auth::id() == $user->id)
				<post></post>
			@endif
	    	<feed :id="{{ $user->id }}"></feed>
		</div>
	</div>
</div>

@endsection